import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.api.java.JavaStreamingContext;

/**
 * Created by root on 04.10.18.
 */
public class MyContext {
    private JavaStreamingContext streamingContext;

    MyContext() {
    SparkConf sparkConf = new SparkConf().setAppName("CSVcounter").setMaster("local")
            .set("spark.executor.memory", "1g")
            .set("spark.driver.memory", "1g")
            .set("spark.driver.maxResultSize", "1g");
    JavaSparkContext sc = new JavaSparkContext(sparkConf);
    this.streamingContext = new JavaStreamingContext(sc, Durations.seconds(2));
    }

    public JavaStreamingContext getStreamingContext() {
        return streamingContext;
    }
}
