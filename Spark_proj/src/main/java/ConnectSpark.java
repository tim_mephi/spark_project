import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.spark.streaming.api.java.JavaInputDStream;
import org.apache.spark.streaming.kafka010.ConsumerStrategies;
import org.apache.spark.streaming.kafka010.KafkaUtils;
import org.apache.spark.streaming.kafka010.LocationStrategies;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by root on 04.10.18.
 */
public class ConnectSpark
{
    public JavaInputDStream<ConsumerRecord<String, String>> stream;
    public String kafkaTopic;
    public MyContext context;
    public String server;
    private Map<String, Object> props;
    ConnectSpark(String topic, MyContext context, String server)
    {
        this.kafkaTopic = topic;
        this.context = context;
        this.server = server;
        Collection<String> topics = Arrays.asList(kafkaTopic);
        Map<String, Object> params = new HashMap();
        params.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, this.server);
        params.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,
                "org.apache.kafka.common.serialization.StringDeserializer");
        params.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,
                "org.apache.kafka.common.serialization.StringDeserializer");
        params.put(ConsumerConfig.GROUP_ID_CONFIG, "group1");
        params.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "latest");
        params.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, true);
        this.props = params;
        this.stream =
                KafkaUtils.createDirectStream(context.getStreamingContext(),
                        LocationStrategies.PreferConsistent(),
                        ConsumerStrategies.<String, String>Subscribe(topics, props)
                );
    }

    public Map<String, Object> getProps() {
        return props;
    }
}
