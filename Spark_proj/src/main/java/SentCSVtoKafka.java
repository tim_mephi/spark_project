import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

/**
 * Created by root on 04.10.18.
 */
public class SentCSVtoKafka
{
    private String sourcePath;
    private ProducerProperty properties;
    private String kafkaTopic;

    SentCSVtoKafka(String pathname, ProducerProperty props, String kafkaTop)
    {
        this.sourcePath = pathname;
        this.properties = props;
        this.kafkaTopic = kafkaTop;
    }

    public String getKafkaTopic() {
        return kafkaTopic;
    }

    public boolean sent() throws FileNotFoundException
    {
        boolean success = true;
        Producer<String, String> producer = null;
        File file = new File(this.sourcePath);
        FileReader fr = new FileReader(file);
        BufferedReader reader = new BufferedReader(fr);
        try {
        producer = new KafkaProducer<>(this.properties.getProps());
        String line = reader.readLine();
        while (line != null)
        {
            producer.send(new ProducerRecord<>(this.kafkaTopic, line));
            System.out.println("Sent: " + line);
            line = reader.readLine();
        }
            }
        catch (Exception e) {
            e.printStackTrace();
            success = false;
            }
        finally {
            producer.close();
            }
    return success;
    }

}
