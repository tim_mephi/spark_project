import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaPairDStream;
import scala.Serializable;
import scala.Tuple2;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * Created by root on 04.10.18.
 */
public class ProcessingCSV implements Serializable
{
    JavaDStream<String> ReduceSpace(JavaDStream stream) {
        return stream.flatMap(new FlatMapFunction<String, String>() {
            @Override
            public Iterator<String> call(String line) throws Exception {
                return Arrays.asList(line.trim()).iterator();
            }
        });
    }
    JavaPairDStream<String,Integer> Calculate(JavaDStream stream) {
        return stream.mapToPair(new PairFunction<String, String, Integer>() {
            @Override
            public Tuple2<String, Integer> call(String word) throws Exception {
                List<String> str = Arrays.asList(word.split("[,]+"));
                return new Tuple2<>(str.get(0), str.size());
            }
        });
    }



}
