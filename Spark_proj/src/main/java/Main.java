import java.io.*;
import org.apache.spark.streaming.api.java.*;


public class Main {

    public static void main(String[] args) throws InterruptedException, IOException {
        //Задаем свойства KafkaProducer
        ProducerProperty props = new ProducerProperty();
        //Отправка csv файла в кафку
        SentCSVtoKafka sent_scv = new SentCSVtoKafka(args[0],
                props, args[1]);
        boolean was_sent = sent_scv.sent();
        //Если был отправлен
        if (was_sent) {
            //Создание контекста
            MyContext context = new MyContext();
            //Создание потока, который будет читать с кафки
            ConnectSpark connectSpark = new ConnectSpark(sent_scv.getKafkaTopic(),context,props.getServer());
            //Создание объекта класса "Получить инстанс CSV из кафки"
            GetInstanceCSV getInstanceCSV = new GetInstanceCSV();
            //Создание объекта класса обработки CSV файла
            ProcessingCSV process = new ProcessingCSV();
            //Получаем строки и на всякий случай убираем пробелы с краев каждой строки
            JavaDStream<String> words = process.ReduceSpace(getInstanceCSV.get_instance(connectSpark));
            //Подсчитываем кол-во ненулевых столбцов в каждой строке
            JavaPairDStream<String,Integer> res = process.Calculate(words);
            //Убираем первую строку(Заголовок CSV файла)
            JavaPairDStream<String, Integer> wordMap = res.filter(word -> !"ID".equals(word._1()));
            //Отправка результата обратно в кафку с каким-то топиком
            SentResult sent_csv = new SentResult("Kafka5");
            sent_csv.sent(wordMap);
            //Старт контекста
            context.getStreamingContext().start();
            try {
                    context.getStreamingContext().awaitTerminationOrTimeout(1000);
                }
            catch (InterruptedException err)
            {}
            //Подписка на топик с результатом
            CreateSubscribe subscribe = new CreateSubscribe(sent_csv.topic,connectSpark.getProps());
            //Запись результата в CSV файл
            WriteCSV writer = new WriteCSV();
            writer.write("output.csv", subscribe.consumer);
        }
        //Если начальный файл не был залит в кафку, завершение программы
        else
            {
                System.exit(0);
            }
    }
}
