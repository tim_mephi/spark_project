import org.apache.kafka.clients.consumer.KafkaConsumer;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * Created by root on 04.10.18.
 */
public class CreateSubscribe
{
    public KafkaConsumer consumer;
    CreateSubscribe(String topic, Map<String, Object>props) {
        this.consumer = new KafkaConsumer(props);
        List<String> topics = new ArrayList<String>();
        topics.add(topic);
        consumer.subscribe(topics);
    }
}
