import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.streaming.api.java.JavaDStream;

import java.io.Serializable;

/**
 * Created by root on 04.10.18.
 */
public class GetInstanceCSV implements Serializable
{

    public JavaDStream<String> get_instance(ConnectSpark connect)
    {
        return connect.stream.map(new Function<ConsumerRecord<String, String>, String>() {
            @Override
            public String call(ConsumerRecord<String, String> kafkaRecord) throws Exception {
                return kafkaRecord.value();
            }
        });

    }
}
