import java.util.Properties;

/**
 * Created by root on 04.10.18.
 */
public class ProducerProperty
{
    private String server = "localhost:9092";
    private Properties props;
    ProducerProperty(String name_server)
    {
        this.props = new Properties();
        this.server = name_server;
        props.put("bootstrap.servers", this.server);
        props.put("acks", "all");
        props.put("retries", 0);
        props.put("batch.size", 16384);
        props.put("linger.ms", 1);
        props.put("buffer.memory", 33554432);
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
    }
    ProducerProperty()
    {
        this.props = new Properties();
        this.server = "localhost:9092";
        props.put("bootstrap.servers", this.server);
        props.put("acks", "all");
        props.put("retries", 0);
        props.put("batch.size", 16384);
        props.put("linger.ms", 1);
        props.put("buffer.memory", 33554432);
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
    }
    public Properties getProps() {
        return props;
    }

    public String getServer() {
        return server;
    }
}
