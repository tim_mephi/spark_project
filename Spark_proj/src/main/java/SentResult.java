import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.spark.streaming.api.java.JavaPairDStream;

import java.io.Serializable;

/**
 * Created by root on 04.10.18.
 */
public class SentResult implements Serializable
{
    public String topic;

    SentResult(String topic)
    {
        this.topic = topic;
    }

    void sent(JavaPairDStream<String, Integer> wordMap2)
    {
        wordMap2.foreachRDD(rdd -> {
            rdd.foreach(record -> {
                ProducerProperty props2 = new ProducerProperty();
                Producer<String, String> connection = new KafkaProducer<String, String>(props2.getProps());
                connection.send(new ProducerRecord<String, String>(this.topic, record._1() + "," + String.valueOf(record._2())));
            });
        });
    }
}
