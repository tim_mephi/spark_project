import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Created by root on 04.10.18.
 */
public class WriteCSV {
 void write(String namefile, KafkaConsumer consumer) throws IOException
 {
     OutputStream output = new FileOutputStream(namefile);
     while (true) {
         ConsumerRecords<String, String> records = consumer.poll(10);
         try {
             for (ConsumerRecord<String, String> record : records) {
                 String st = record.value() + '\n';
                 output.write(st.getBytes());
             }
         } catch (IOException e) {
             e.printStackTrace();
         }
     }
 }
}
